# -*- coding: utf-8 -*-
import resources
from PyQt4 import QtCore, QtGui
from FloodLosses_ui import Ui_MainWindow
from qgis.core import *


class TestPlugin:
    def __init__(self, iface):
        # save reference to the QGIS interface
        self.iface = iface
        self.MainWindow = QtGui.QMainWindow()
        self.ui = Ui_MainWindow()

    def initGui(self):

        # create action that will start plugin configuration
        self.action = QtGui.QAction(QtGui.QIcon(":/test/icons/icon.png"), "Test plugin", self.iface.mainWindow())
        self.action.setObjectName("testAction")
        self.action.setWhatsThis("Configuration for test plugin")
        self.action.setStatusTip("This is status tip")
        QtCore.QObject.connect(self.action, QtCore.SIGNAL("triggered()"), self.run)
        # add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu("&Test plugins", self.action)
        # connect to signal renderComplete which is emitted when canvas
        # rendering is done

    def unload(self):
        # remove the plugin menu item and icon
        self.iface.removePluginMenu("&Test plugins", self.action)
        self.iface.removeToolBarIcon(self.action)
        # disconnect form signal of the canvas

    def run(self):
        # create and show a configuration dialog or something similar

        self.ui.setupUi(self.MainWindow)
        self.MainWindow.show()
