import sip
try:
    apis = ["QDate", "QDateTime", "QString", "QTextStream", "QTime", "QUrl", "QVariant"]
    for api in apis:
        sip.setapi(api, 2)
except ValueError:
    pass


from PyQt4 import QtCore, QtGui
from qgis.gui import QgsFieldComboBox
from qgis.gui import QgsMapLayerComboBox


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("Dialog"))
        MainWindow.resize(515, 320)
        self.FieldComboBox = QgsFieldComboBox(MainWindow)
        self.FieldComboBox.setGeometry(QtCore.QRect(140, 110, 160, 27))
        self.FieldComboBox.setObjectName(_fromUtf8("FieldComboBox"))
        self.CrossectionComboBox = QgsMapLayerComboBox(MainWindow)
        self.CrossectionComboBox.setGeometry(QtCore.QRect(140, 40, 160, 27))
        self.CrossectionComboBox.setObjectName(_fromUtf8("CrossectionComboBox"))
        self.DEMComboBox = QgsMapLayerComboBox(MainWindow)
        self.DEMComboBox.setGeometry(QtCore.QRect(140, 180, 160, 27))
        self.DEMComboBox.setObjectName(_fromUtf8("DEMComboBox"))
        self.resultLayerlineEdit = QtGui.QLineEdit(MainWindow)
        self.resultLayerlineEdit.setGeometry(QtCore.QRect(330, 40, 171, 27))
        self.resultLayerlineEdit.setObjectName(_fromUtf8("resultLayerlineEdit"))
        self.crossectionLabel = QtGui.QLabel(MainWindow)
        self.crossectionLabel.setGeometry(QtCore.QRect(140, 10, 181, 17))
        self.crossectionLabel.setObjectName(_fromUtf8("crossectionLabel"))
        self.fieldLabel = QtGui.QLabel(MainWindow)
        self.fieldLabel.setGeometry(QtCore.QRect(140, 80, 171, 17))
        self.fieldLabel.setObjectName(_fromUtf8("fieldLabel"))
        self.demLabel = QtGui.QLabel(MainWindow)
        self.demLabel.setGeometry(QtCore.QRect(140, 150, 141, 17))
        self.demLabel.setObjectName(_fromUtf8("demLabel"))
        self.resultLabel = QtGui.QLabel(MainWindow)
        self.resultLabel.setGeometry(QtCore.QRect(330, 10, 131, 17))
        self.resultLabel.setObjectName(_fromUtf8("resultLabel"))
        self.loadButton = QtGui.QPushButton(MainWindow)
        self.loadButton.setGeometry(QtCore.QRect(10, 40, 99, 27))
        self.loadButton.setObjectName(_fromUtf8("loadButton"))
        self.runButton = QtGui.QPushButton(MainWindow)
        self.runButton.setGeometry(QtCore.QRect(10, 80, 99, 27))
        self.runButton.setObjectName(_fromUtf8("runButton"))
        self.ExtentMaskComboBox = QgsMapLayerComboBox(MainWindow)
        self.ExtentMaskComboBox.setGeometry(QtCore.QRect(140, 250, 160, 27))
        self.ExtentMaskComboBox.setObjectName(_fromUtf8("ExtentMaskComboBox"))
        self.extentLabel = QtGui.QLabel(MainWindow)
        self.extentLabel.setGeometry(QtCore.QRect(140, 220, 141, 17))
        self.extentLabel.setObjectName(_fromUtf8("extentLabel"))

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.crossectionLabel.setText(_translate("Dialog", "Select crossection layer", None))
        self.fieldLabel.setText(_translate("Dialog", "Select water level field", None))
        self.demLabel.setText(_translate("Dialog", "Select DEM layer", None))
        self.resultLabel.setText(_translate("Dialog", "Result layer name", None))
        self.loadButton.setText(_translate("Dialog", "Load layers", None))
        self.runButton.setText(_translate("Dialog", "Run", None))
        self.extentLabel.setText(_translate("Dialog", "Select extent mask", None))


# if __name__ == "__main__":
#     import sys
